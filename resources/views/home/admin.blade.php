<!DOCTYPE HTML>
<html>
<head>
<title>Admin</title>
<meta charset="UTF-8" />
<meta name="Designer" content="PremiumPixels.com">
<meta name="Author" content="$hekh@r d-Ziner, CSSJUNTION.com">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/reset.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/structure.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.countdownTimer.css')}}">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="{{asset('assets/js/jquery-2.0.3.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.countdownTimer.min.js')}}"></script>
</head>



<body>

<div class="row">
	<div class="col-sm-1">

	</div>

	<div class="col-md-10 wrapper">
		<div class ="row">
			<div class="col-md-12">

		    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="float: right;">Logout</a>
		     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
		     </div>
		</div>
		<br><br>

		<div class="row">

			<div class="col-md-12">
				<h4>Data Nasabah</h4>
				<table class="table table-bordered">
					<tr>
						<td>ID</td>
						<td>Nama</td>
						<td>Alamat</td>
						<td>Penghasilan</td>
						<td>Pekerjaan</td>
						<td>NPWP</td>
						<td>Harga</td>
						<td>Action</td>

					</tr>

					<?php foreach($data['nasabah'] as $nasabah){ ?>

					<tr>
						<td>{{$nasabah->id}}</td>
						<td>{{$nasabah->nama}}</td>
						<td>{{$nasabah->alamat}}</td>
						<td><?php echo number_format($nasabah->penghasilan,2,",","."); ?></td>
						<td>{{$nasabah->pekerjaan}}</td>
						<td><img src="{{asset('images').'/'.$nasabah->npwp_link}}"" style="width:100px;height:100px;"></img></td>
						<td><?php echo number_format($nasabah->harga,2,",","."); ?></td>
						<td>

						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Edit</button>

				<!-- Modal -->
				            <div id="myModal" class="modal fade" role="dialog">
				              <div class="modal-dialog">

				                <!-- Modal content-->
				                <div class="modal-content" style="padding: 10px;">
				                 
				                    <button type="button" class="close" data-dismiss="modal">&times;</button>
				                    
				                		 <form method="POST" action="{{ url('/post_editNasabah') }}" enctype="multipart/form-data">
					                     {{ csrf_field() }}
					                     <input type="hidden" name="id" value="{{$nasabah->id}}">
					                        <div class="form-group">
					                            <label>Name</label>
					                            <input value="{{$nasabah->nama}}" class="form-control" type="text" name="nama" required>

					                        </div>
					                        <div class="form-group">
					                            <label>Alamat</label>
					                            <textarea class="form-control" name="alamat" required> {{$nasabah->alamat}} </textarea> 

					                        </div>
					                        <div class="form-group">
					                            <label>Penghasilan Bersih</label>
					                            <input value="{{$nasabah->penghasilan}}" class="form-control" type="text" name="penghasilan" onkeypress="return isNumberKey(event)" required>

					                        </div>
					                        <div class="form-group">
					                            <label>Pekerjaan</label>
					                            <input value="{{$nasabah->pekerjaan}}" class="form-control" type="text" name="pekerjaan" required>

					                        </div>
					                        <div class="form-group">
					                            <label>NPWP</label>
					                            <input class="form-control" type="file" name="npwp">

					                        </div>
					                         <div class="form-group">
					                            <label>Harga Rumah</label>
					                            <input value="{{$nasabah->harga}}" class="form-control" type="text" name="harga" onkeypress="return isNumberKey(event)" required>

					                        </div>

					                         <div class="form-group">
					                            
					                            <input type="submit" value="submit">

					                        </div>


					                    </form>


				                 
				                    
				                </div>

				              </div>
				            </div>


				        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2">Delete</button>

				<!-- Modal -->
				            <div id="myModal2" class="modal fade" role="dialog">
				              <div class="modal-dialog">

				                <!-- Modal content-->
				                <div class="modal-content" style="padding: 10px;">
				                 
				                    <button type="button" class="close" data-dismiss="modal">&times;</button>
				                    <center><h4 class="modal-title">Apakan Anda Yakin ?</h4></center><br><br>
				                  
				                 
				                 
				                    <center>
				                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>


				                        <a href="/admin" class="btn btn-success" onclick="event.preventDefault(); document.getElementById('logout-delete').submit();">Ya</a>

				                        <form id="logout-delete" action="{{ url('/post_deleteNasabah') }}" method="POST" style="display: none;">{{ csrf_field() }} <input type="hidden" name="id" value="{{$nasabah->id}}"></form>
				                        <br>

				                    </center>
				                  
				                </div>

				              </div>
				            </div>

							



						</td>

					</tr>


					<?php } ?>


				</table>

			</div>

		</div>

	</div>



</div>



</body>