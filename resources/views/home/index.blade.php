<!DOCTYPE HTML>
<html>
<head>
<title>Simple Login Form</title>
<meta charset="UTF-8" />
<meta name="Designer" content="PremiumPixels.com">
<meta name="Author" content="$hekh@r d-Ziner, CSSJUNTION.com">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/reset.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/structure.css')}}">
</head>

<body>
<form class="box login" method="POST" action="/post_login">
{{ csrf_field() }}
	<fieldset class="boxBody">
	  <label>Username</label>
	  <input type="text" tabindex="1" required name="username">
	  <label>Password</label>
	  <input type="password" tabindex="2" required name="password">
	</fieldset>
	<footer>
	  
	  <input type="submit" class="btnLogin" value="Login" tabindex="4">
	  <!-- <a href="/soal" class="btnLogin">Login</a> -->
	</footer>
</form>

</body>
</html>
