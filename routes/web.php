<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=>'NasabahController@getIndex']);
Route::get('/admin',['as'=>'login','uses'=>'HomeController@getIndex']);

Route::post('post_nasabah','NasabahController@postNasabah');
Route::post('post_editNasabah','HomeController@postEditNasabah');
Route::post('post_deleteNasabah','HomeController@postDeleteNasabah');




Auth::routes();

Route::get('/home', 'HomeController@index');
