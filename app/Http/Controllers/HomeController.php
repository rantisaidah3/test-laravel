<?php

namespace App\Http\Controllers;
use App\Models\m_nasabah as m_nasabah;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

     
    }

    public function getIndex(){

        $nasabah = m_nasabah::get();
        $data =array("nasabah" => $nasabah);

         return view('home.admin')->with("data",$data);
    }

    public function postEditNasabah(Request $request){

        $nama = $request->input('nama');
        $alamat = $request->input('alamat');
        $penghasilan = $request->input('penghasilan');
        $pekerjaan = $request->input('pekerjaan');
        $harga = $request->input('harga');
        $id = $request->input('id');

        $nasabah = m_nasabah::find($id);

        if(!empty($request->npwp)){

            $imageName = time().'.'.$request->npwp->getClientOriginalExtension();
            $request->npwp->move(public_path('images'), $imageName);

            $npwp_link = $imageName;
        }else{

            $npwp_link = $nasabah->npwp_link;
        }

        $nasabah->nama = $nama;
        $nasabah->alamat = $alamat;
        $nasabah->penghasilan = $penghasilan;
        $nasabah->pekerjaan = $pekerjaan;
        $nasabah->npwp_link = $npwp_link;
        $nasabah->harga = $harga;

        $nasabah->save();

       return redirect('/admin');


    }

    public function postDeleteNasabah(Request $request){

         $id = $request->input('id');
          $nasabah = m_nasabah::where('id',$id)->delete();
        return redirect('/admin');


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response


     */

    
}
