<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\m_nasabah as m_nasabah;


class NasabahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function getIndex(){

         return view('home.nasabah');
    }

    public function postNasabah(Request $request){

        $this->validate($request, [
            'npwp' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $nama = $request->input('nama');
        $alamat = $request->input('alamat');
        $penghasilan = $request->input('penghasilan');
        $pekerjaan = $request->input('pekerjaan');
        $harga = $request->input('harga');

        $imageName = time().'.'.$request->npwp->getClientOriginalExtension();
        $request->npwp->move(public_path('images'), $imageName);

        $npwp_link = $imageName;

        $nasabah = new m_nasabah();
        $nasabah->nama = $nama;
        $nasabah->alamat = $alamat;
        $nasabah->penghasilan = $penghasilan;
        $nasabah->pekerjaan = $pekerjaan;
        $nasabah->npwp_link = $npwp_link;
        $nasabah->harga = $harga;

        $nasabah->save();


        if($penghasilan >= 7000000){

            $persen = '0%';
            $dp = 0;
            $utang = $harga;
        }else{
            
            $persen = '30%';
            $dp = ($harga/100)*30;
            $dp = number_format($dp,2,",",".");
            $utang  = $harga - $dp;
        }

        $bulan = 15*12;
        $cicilan = $utang/$bulan;

        $cicilan = number_format($cicilan,2,",",".");

        $data = array(

            "persen" => $persen,
            "dp" => $dp,
            "utang" => $utang,
            "cicilan" => $cicilan

        );



        return view('home.cicilan')->with("data",$data);
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response


     */

    
}